Testcases
=========

What?
-----

This file contains human-readable descriptions of included test cases. Ideal for in-detail information about various caveats in testcases. Leave this text here and add your stuff below.

Naming conventions
------------------

### Suffixes
Each testcase should have an input file (suffixed with `.in`) and sample output file (suffixed with `.ou`)

### Prefixes
- `O.` for testcases in assignment
- `H.` for manually created testcases
- `R.` for testcases generated randomly
- `C.` for testcases with caveats

Test Cases
------------

### CaseTemplate (use file name)
Brief description.
