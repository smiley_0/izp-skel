/**
 * @file main.c
 *
 * XXX Project XX
 *
 * @author    Adrian Kiraly (xkiral01@stud.fit.vutbr.cz) [1BIA]
 * @date      YYYY-MM-DD
 * @version   1.0
 *
 * @todo Fill header
 * @todo Brief description
 */

#include <stdio.h>
#include <stdlib.h>

// Constants
// =============================================================================

// Enums
// =============================================================================

// Localization strings
// =============================================================================

// Functions
// =============================================================================

// Output functions
// =============================================================================

/**
 * @brief      Prints help message
 */
void print_help(const char *binary_name) {
    printf(
    "usage: %s [anything]\n\n"
    "Change me!\n"
    "-----------------------------------------------------------------------\n"
    "XXX Project XX / ???\n\n"
    "version 1.0                                        Adrian Kiraly [1BIA]\n"
    "compiled on: %s %s            xkiral01@stud.fit.vutbr.cz\n",
    binary_name, __DATE__, __TIME__);
}


// Main function
// =============================================================================

/**
 * @brief      Main function
 *
 * @param[in]  argc  Argument count
 * @param[in]  argv  Argument vector (unused)
 *
 * @return     Return code
 */
int main(int argc, char const *argv[])
{
    (void) argc; (void) argv;


    return EXIT_SUCCESS;
}
